/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "thread_local.h"
#include <new>

#ifdef _WIN32
 #define WIN32_LEAN_AND_MEAN
 #include <windows.h>
#else
 #include <pthread.h>
#endif

#ifdef _WIN32
 #define TLS_HANDLE_PTR reinterpret_cast<DWORD *>(m_Handle)
#else
 #define TLS_HANDLE_PTR reinterpret_cast<pthread_key_t *>(m_Handle)
#endif

Thread::Local::Local()
{
  #ifdef _WIN32
	m_Handle = new DWORD(handle);
	*TLS_HANDLE_PTR = TlsAlloc();
	if (*TLS_HANDLE_PTR == TLS_OUT_OF_INDEXES)
	{
		delete TLS_HANDLE_PTR;
		throw std::bad_alloc();
	}
  #else
	m_Handle = new pthread_key_t;
	int r = pthread_key_create(TLS_HANDLE_PTR, NULL);
	if (r != 0)
	{
		delete TLS_HANDLE_PTR;
		throw std::bad_alloc();
	}
  #endif
}

Thread::Local::~Local()
{
  #ifdef _WIN32
	TlsFree(*TLS_HANDLE_PTR);
  #else
	pthread_key_delete(*TLS_HANDLE_PTR);
  #endif
	delete TLS_HANDLE_PTR;
}

void * Thread::Local::rawGet() const
{
  #ifdef _WIN32
	return TlsGetValue(*TLS_HANDLE_PTR);
  #else
	return pthread_getspecific(*TLS_HANDLE_PTR);
  #endif
}

void Thread::Local::set(void * ptr)
{
  #ifdef _WIN32
	TlsSetValue(*TLS_HANDLE_PTR, ptr);
  #else
	pthread_setspecific(*TLS_HANDLE_PTR, ptr);
  #endif
}
