/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "thread.h"
#include <stdexcept>
#include <iomanip>
#include <sstream>

#ifdef _WIN32
 #define WIN32_LEAN_AND_MEAN
 #include <windows.h>
#else
 #include <pthread.h>
#endif

#ifdef _WIN32
 #define THREAD_HANDLE_PTR reinterpret_cast<HANDLE *>(m_Handle)
#else
 #define THREAD_HANDLE_PTR reinterpret_cast<pthread_t *>(m_Handle)
#endif

namespace
{
	struct ThreadContext
	{
		Thread::PFNTHREADPROC proc;
		void * context;

		inline ThreadContext(Thread::PFNTHREADPROC p, void * c) : proc(p), context(c) {}
	};
}

static ThreadContext getThreadContext(void * parameter)
{
	ThreadContext * p = reinterpret_cast<ThreadContext *>(parameter);
	ThreadContext ctx = *p;
	delete p;
	return ctx;
}

#ifndef _WIN32
static void * threadEntry(void * parameter)
{
	ThreadContext ctx = getThreadContext();
	ctx.proc(ctx.context);
	return NULL;
}
#else
static DWORD WINAPI threadEntry(LPVOID lpParameter)
{
	ThreadContext ctx = getThreadContext();
	ctx.proc(ctx.context);
	return NULL;
}
#endif

Thread::Handle::Handle()
	: m_Valid(false)
{
  #ifdef _WIN32
	m_Handle = new HANDLE;
  #else
	m_Handle = new pthread_t;
  #endif
}

Thread::Handle::~Handle()
{
	detach();
	delete THREAD_HANDLE_PTR;
}

void Thread::Handle::start(PFNTHREADPROC proc, void * context)
{
	detach();

  #ifdef _WIN32
	ThreadContext * ctx = new ThreadContext(proc, context);
	*THREAD_HANDLE_PTR = CreateThread(NULL, 0, threadEntry, ctx, 0, NULL);
	if (!*THREAD_HANDLE_PTR)
	{
		DWORD err = GetLastError();
		delete ctx;
		std::stringstream ss;
		ss << "Unable to start thread (code 0x" << std::hex << std::setw(8) << std::setfill('0') << err << ").";
		throw std::runtime_error(ss.str());
	}
  #else
	ThreadContext * ctx = new ThreadContext(proc, context);
	int r = pthread_create(THREAD_HANDLE_PTR, NULL, threadEntry, ctx);
	if (r != 0)
	{
		delete ctx;
		std::stringstream ss;
		ss << "Unable to start thread (code " << r << ").";
		throw std::runtime_error(ss.str());
	}
  #endif
}

void Thread::Handle::join()
{
	if (m_Valid)
	{
	  #ifdef _WIN32
		WaitForSingleObject(*THREAD_HANDLE_PTR, INFINITE);
		CloseHandle(*THREAD_HANDLE_PTR);
	  #else
		pthread_join(*THREAD_HANDLE_PTR);
	  #endif
		m_Valid = false;
	}
}

void Thread::Handle::detach()
{
	if (m_Valid)
	{
	  #ifdef _WIN32
		CloseHandle(*THREAD_HANDLE_PTR);
	  #else
		pthread_detach(*THREAD_HANDLE_PTR);
	  #endif
		m_Valid = false;
	}
}
