
SUBPROJECT("cxx-threads")

DEFINES {
	"THREAD_H=\"" .. CURRENT_SOURCE_DIR .. "/thread.h\"",
	"THREAD_LOCAL_H=\"" .. CURRENT_SOURCE_DIR .. "/thread_local.h\"",
}

SOURCE_FILES {
	"thread.cpp",
	"thread.h",
	"thread_local.cpp",
	"thread_local.h",
}
